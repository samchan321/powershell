#search globally
function GS ($Path, $Search, $Case) {
    #this search is case insensitive
    $TestPath = Test-Path -Path "~\GSLog\"
    if ($TestPath -eq  [system.convert]::ToBoolean("False")) {
        #mkdir ~\GSLog\  -->not working
        Write-Host "folder GSLog does not exist"
        New-Item -Path ~\GSLog\ -ItemType Directory
    }
    $TestPath = Test-Path -Path "~\GSLog\"
    if ($TestPath -eq  [system.convert]::ToBoolean("False")) {
        echo "Cannot create a folder ~\GSLog. Please check if you have the privilege."
    }
    $CurDate = Get-Date -UFormat %Y%m%d%H%M
    $CurSec = Get-Date| %{ $_.Second}
    #yyyyMMddTHHmmssffff
    #Get-ChildItem test.txt -Recurse |Select-String -Pattern "a" | %{echo "Path           : $($_.Path)" "    LineNumber : $($_.LineNumber)" "        Line   : $($_.Line)"} |Out-File "~\GSLog\LastSearch.txt"
    if ($Case -eq "c") {
        $Matches = Get-ChildItem $Path -Recurse |Select-String -Pattern $Search -CaseSensitive
    } else {
        $Matches = Get-ChildItem $Path -Recurse |Select-String -Pattern $Search
    }
    $MatchCount = "Number of Matches - $($Matches.matches.count)" 
    $MatchCount | Out-File "~\GSLog\LastSearch.txt"
    $Matches | %{echo "Path           : $($_.Path)" "    LineNumber : $($_.LineNumber)" "        Line   : $($_.Line)"} |Add-Content "~\GSLog\LastSearch.txt"
    
    #Get-ChildItem $Path -Recurse |Select-String -Pattern $String |Format-List -Property Line, LineNumber,Path |Out-File "~\GSLog\LastSearch.txt"
    copy "~\GSLog\LastSearch.txt" "~\GSLog\GS$CurDate$CurSec.txt"
    Write-Host $MatchCount
    Write-Host "GS-Help -- get help for Global Search command"
    Write-Host "GS-Cat -- cat the Search result"

}

function GS-Help(){
    Write-Host "GS-Cat -- cat the Search result"
    Write-Host "GS-ReadLog -- Vim the Search result"
    Write-Host "GS-EditLog -- Edit Search result"
    Write-Host "GS-StartLog -- Edit Search result"
    Write-Host "GS-Replace -- Repalce content in file recursively in folders"
}


function GS-Cat(){
    cat "~\GSLog\lastsearch.txt"
}

function GS-ReadLog(){
    vim -R "~\GSLog\lastsearch.txt"
}

function GS-EditLog(){
    vim "~\GSLog\lastsearch.txt"
}

function GS-StartLog(){
    start vim "~\GSLog\lastsearch.txt"
}

function GS-Replace($Path, $Search, $Replace, $case) {
    #this is case-insensitive
    if ($case -eq "c") {
        #this is case-sensitive
        get-childItem $Path -recurse | % `
        {
            $filepath = $_.FullName;
            (get-content $filepath) |
                % { $_ -creplace $Search, $Replace } |
                set-content $filepath -Force
        }    
    } else {
        get-childItem $Path -recurse | % `
        {
            $filepath = $_.FullName;
            (get-content $filepath) |
                % { $_ -replace $Search, $Replace } |
                set-content $filepath -Force
        }
    }
}
