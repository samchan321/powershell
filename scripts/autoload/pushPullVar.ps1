#push variables
function push () {
#    (Get-Variable *).Name | Select-String -Pattern "^v|^l" | Export-Clixml "~\vars.xml"
    Get-Variable * | Where-Object { $_.Name -Match "^l|^v"} | Export-Clixml "~\vars.xml"
    Get-Variable * | Where-Object { $_.Name -Match "^l|^v"} 
}

#pull variables
function pull () {
    Import-Clixml "~\vars.xml" | %{ Set-Variable $_.Name $_.Value }
    Get-Variable * | Where-Object { $_.Name -Match "^l|^v"} 
}
